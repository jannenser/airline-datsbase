#include <stdio.h>
#include "sqlite3.h"

#define __debugbreak() __asm int 3
#pragma warning(disable : 4996)

int main(void) {
FILE *f;
void *pData;
int i;
sqlite3 *db;
long nSize;
const char *pSql;
const char *pSqlEnd;
sqlite3_stmt *st;
int nColCnt;
int err_nr;

// ������ ���� ������� � ������

f = fopen("SCRIPT.SQL.txt", "rb");
if (!f) __debugbreak();

fseek(f, 0, 2);
nSize = ftell(f);

pData = malloc(nSize);
if (!pData) __debugbreak();


fseek(f, 0, 0);
i = fread(pData, nSize, 1, f);
if (i != 1)
{
  __debugbreak();
}
fclose(f);
// ��������� ��

err_nr = sqlite3_open(
  "MYDB",
  &db);
  if (err_nr != SQLITE_OK) __debugbreak();
  
  pSql = (const char*)pData;
  pSqlEnd = pSql + nSize;
  for (;;) {
    err_nr = sqlite3_prepare(
    db,
    pSql,
    pSqlEnd - pSql,
    &st,
    &pSql);
  if (err_nr != SQLITE_OK) __debugbreak();
  if (!st) break;

  nColCnt = sqlite3_column_count(st);
  for (i = 0; i < nColCnt; i++) {
    if (i) printf("\t");
    printf("%s", sqlite3_column_name(st, i));
  }
  printf("\n");

  while ((err_nr = sqlite3_step(st)) == SQLITE_ROW)
  {
    for (i = 0; i < nColCnt; i++) {
      if (i) printf("\t");
      printf("%s", sqlite3_column_text(st, i));
    }
    printf("\n");
  }
  if (err_nr != SQLITE_DONE) __debugbreak();

  err_nr = sqlite3_finalize(st);
  if (err_nr != SQLITE_OK) __debugbreak();
  }

  err_nr = sqlite3_close(db);
  if (err_nr != SQLITE_OK) __debugbreak();
  
  system("pause");
 }
